# Semis de Tomates

## Variétés

* Favoriser les variétés précosses

## Période

* Tôt dans l'année

## Pots

* Plutot de grande taille

## Quantité

* Env 20 plants