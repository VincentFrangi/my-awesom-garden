# Bouture de groseilles

## Variétés à bouturer

* Blanche
* Rouge (résinets)
* Cassis
* Groseilles à maquereaux

## Emplacement

Future butte de culture ouest

## Période

Juin - Septembre

![Explication](https://www.youtube.com/watch?v=pqZmydXu5LU)