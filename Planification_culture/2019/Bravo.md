# Bravo [B]

Orienté sud mais derrière des petits arbres
Butte avec platte bande surélevée en noisiter

Couches:

1. Bois en décomposition
2. Terre
3. Mulch
    * Paille
    * Foin
    * Broyat
4. Tonte

## Planté

* Salade hiver
* Salade rouge
* Fraises
* Brassicassé
    * Choux [Monté en graine] (Moche)
* Ail
* Ognons
* Petits pois (Pas germé)

## A planter

* Amarantacés
    * Blettes
    * Bettraves
    * Epinards
* Piments


