# Arabidopsis [D]

Orienté Sud - Bien exposé

> Rajouter de la matière azotée
> Bien l'arroser

## Planté

* Salade d'hiver
* Salade rouge
* Aliacés
    * Oignons
    * Ail
    * Poireaux
* [Mort] Légume rouge bizarre (famille des choux)
* Petits pois

## À Planter

* Apiacés
    * Carottes

* Solenacés
    * Tomates
    * Piments
    * Aubergines