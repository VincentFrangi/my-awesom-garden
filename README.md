# Jardin

L'idée et d'organiser de mieux organiser le jardin


## Contenu du repository

* [Taches](./Taches)
    - Contien les différentes tâches à faire au jardin
* [Ressources](./Ressources)
    - Contien des notes
    - Contient des liens vers des information utiles

## Plan du jardin

![](./Plan/Plan.png)